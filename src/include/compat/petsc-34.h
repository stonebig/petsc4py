#ifndef PETSC4PY_COMPAT_34_H
#define PETSC4PY_COMPAT_34_H

#include "petsc-private/isimpl.h"
#include "petsc-private/vecimpl.h"
#include "petsc-private/matimpl.h"
#include "petsc-private/dmimpl.h"
#include "petsc-private/kspimpl.h"
#include "petsc-private/pcimpl.h"
#include "petsc-private/snesimpl.h"
#include "petsc-private/tsimpl.h"

#include "petsc-34/petsc.h"
#include "petsc-34/petscvec.h"
#include "petsc-34/petscmat.h"
#include "petsc-34/petscksp.h"
#include "petsc-34/petscsnes.h"
#include "petsc-34/petscdm.h"

#endif/*PETSC4PY_COMPAT_34_H*/
